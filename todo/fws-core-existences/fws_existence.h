/*
Copyright 2017, Matheus H. Silva.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#ifndef FWS_EXISTENCE
#define FWS_EXISTENCE

#include "../fws-core-native/fws_atribute.h"

//#struct fws_ds_existence
typedef struct fws_ds_existence
{
	char *fws_unique;
	void *fws_p_address;
	unsigned short fws_mem_props;
	char *fws_verbose;
	FWS_DS_ATRIBUTE *fws_atribs;	
} FWS_DS_EXISTENCE;
//#struct fws_ds_existence

//#FWS_DS_EXISTENCE *fws_new_existence
const FWS_DS_EXISTENCE *fws_new_existence(char *, void *, unsigned short, char *, FWS_DS_ATRIBUTE *);
//#FWS_DS_EXISTENCE *fws_new_existence

#endif
