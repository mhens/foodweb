/*
Copyright 2017, Matheus H. Silva.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#ifndef FWS_IO
#define FWS_IO

#include "../fws-core-truth/fws_checkout.h"

#define FWS_MAX_CHARS_PER_LINE (unsigned char)86

//#typedef struct fws_ds_src_file
typedef struct fws_ds_src_file
{
	char *fws_file_name;
	unsigned int fws_num_lines;
	unsigned int fws_file_size;
	char **fws_raw_lines;
} FWS_DS_SRC_FILE;
//#typedef struct fws_ds_src_file

//#char *fws_read_just_line_at
char *fws_read_just_line_at(unsigned short, const FILE *);
//#char *fws_read_just_line_at

//#const FWS_DS_SRC_FILE *fws_open_it
const FWS_DS_SRC_FILE *fws_open_it(char *);
//#const FWS_DS_SRC_FILE *fws_open_it

#endif
