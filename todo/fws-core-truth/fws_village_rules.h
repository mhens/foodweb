/*
Copyright 2017, Matheus H. Silva.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#ifndef FWS_VILLAGE_RULES
#define FWS_VILLAGE_RULES

#include "fws_crucial_h.h"

#define FWS_TOP_VILLAGE_TOKEN '{'
#define FWS_BOTTOM_VILLAGE_TOKEN '}'
#define FWS_BORDER_VILLAGE_TOKEN ';'

//#typedef struct fws_ds_village_rules
typedef struct fws_ds_village_rules
{
	char fws_valid_top_token;
	char fws_valid_bottom_token;
	char fws_valid_border_token;
} FWS_DS_VILLAGE_RULES; 
//#typedef struct fws_ds_village_rules

//#void fws_specify_village_rules
void fws_specify_village_rules(FWS_DS_VILLAGE_RULES *);
//#void fws_specify_village_rules

//#unsigned char fws_integrity_by_village
unsigned char fws_integrity_by_village(const char *, FWS_DS_VILLAGE_RULES *);
//#unsigned char fws_integrity_by_village

#endif
